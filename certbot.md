# certbot

## запуск certbot с помощью конфигурационного файла

```ini
domain = mysite.ru
domain = www.mysite.ru
authenticator = webroot
webroot-path = /home/sites/mysite/www

# всегда обновлять
#force-renewal = true

# обновлять если осталось менее 30 дней (вроде)
keep-until-expiring = true

noninteractive = true
renew-hook = /usr/sbin/nginx -s reload
```

Запуск:
```bash
certbot -c /path/to/config.ini certonly
```